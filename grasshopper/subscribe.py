from google.cloud import pubsub_v1
from comtypes.client import CreateObject
import requests
import time
import json
import threading
import os
import itertools
import traceback
import base64

# TODO proper recovery and checking
doneFlag = threading.Event()
runFlag = threading.Event()
killFlag = threading.Event()
outputUrl = ''
scriptPath = ''

SCRIPT_DIR = 'C:\\Users\\Public\\gh_load'  # SCRIPT TO GO HERE
INPUT_PATH = 'C:\\Users\\Public\\gh_load\\gmodel.json'
OUTPUT_PATH = 'C:\\Users\\Public\\gh_dump\\output.json'
RESTART_PATH = 'C:\\Users\\Public\\test_plugin\\start_subscribe.bat'
SUBSCRIPTION_PATH = 'projects/giraffe-feaso/subscriptions/rhino'

# Error handling
def fail_task(response_json, kill=True):
    global doneFlag, outputUrl, SCRIPT_DIR
    try:
        requests.post(outputUrl, json=response_json)
    finally:
        doneFlag.set()
        runFlag.clear()
        # wait for message to ack. The task will not be reattempted
        time.sleep(2)
        if kill:
            os.system(RESTART_PATH)

# Subscription callback
def receive_message(message):
    print('Received message: {}'.format(message))
    global doneFlag, runFlag, killFlag, outputUrl, scriptPath, SCRIPT_DIR, INPUT_PATH
    try:
        # Get message payload
        payload = requests.get(message.attributes['input_url'].replace('plugins-dot-', '')).json()
        scriptPath = os.path.join(
            SCRIPT_DIR,
            f'{message.attributes["name"].lower()}.gh'
        )
        if not os.path.isfile(scriptPath):
            fail_task({'failure_message': f'No Grasshopper script ${scriptPath} found','traceback': ''}, kill=False)
       
        # Save input data model so it can be read by Grasshopper Script
        with open(INPUT_PATH, 'w') as f:
            json.dump(payload, f)

        # Set runFlag and wait for doneFlag
        outputUrl = message.attributes['output_url']
        runFlag.set()
        while not doneFlag.is_set():
            time.sleep(0.3)
            if killFlag.is_set():
                message.nack()
                return
        doneFlag.clear()
        message.ack()

    except Exception as e:
        print(traceback.format_exc())
        message.ack()
        fail_task({'failure_message': 'Invalid input. Script did not run.',
                   'traceback': traceback.format_exc}, kill=False)


# PubSub connection
subscriber = pubsub_v1.SubscriberClient()

# Subscribe and only process 1 message at a time
subscriber.subscribe(
    SUBSCRIPTION_PATH,
    callback=receive_message,
    flow_control=pubsub_v1.types.FlowControl(max_messages=1)
)


def spinner():
    l = ['-', '\\', '|', '/']
    for c in itertools.cycle(l):
        yield(c)
spinner_gen = spinner()

rhino = CreateObject('Rhino.Application')
print('Listening..')

# This has to be in the main program because pythonCOM does not let threads (like the subscribe callback) use the Rhino application object
while True:
    time.sleep(0.3)
    print(f'\r{next(spinner_gen)}', end='')
    if runFlag.is_set():
        # Start Kill Timer in case script hangs the server
        kill_thread = threading.Timer(
            60,
            fail_task,
            args=[
                {'failure_message': "Script was taking too long to run. Are you trying to use a grasshopper plugin that is not installed on the server?"}
            ])
        kill_thread.start()
        try:
            print('processing...\r')
            print(scriptPath)
            rhino.runscript(f'-grasshopper d o {scriptPath} !', False)
            # close document
            rhino.runscript('-grasshopper d l !', False)
            # write output
            if os.path.exists(OUTPUT_PATH):
                with open(OUTPUT_PATH, 'r') as f:
                    output_data = json.load(f)
                if 'pngPath' in output_data['appData']:
                    # Encode PNG file as a string and add it to the JSON response
                    with open(output_data['appData']['pngPath'], 'rb') as f:
                        png_bytes = f.read()
                    output_data['appData']['image'] = {}
                    output_data['appData']['image']['png'] = base64.b64encode(
                        png_bytes).decode()
                    output_data['appData'].pop('pngPath')
                    output_json = output_data

                output_json = output_data

            else:  # script didn't dump anything (to the right location anyway)
                print('No output file written...')
                output_json = {
                    'failure_message': f"Grasshopper Script Failed to write output data. Ensure data is put in {OUTPUT_PATH}"}

            # respond with output of script
            r = requests.post(outputUrl, json=output_json)
            print(r)
            # clean up
            
        except KeyboardInterrupt as e:
            killFlag.set()
            time.sleep(5)
            del rhino
            raise
        except Exception as e:
            print(traceback.format_exc())
            killFlag.set()
            time.sleep(5)  # let exception traceback be viewed and nack message
            os.system(RESTART_PATH)
        finally:
            print('DONE!......')
            doneFlag.set()
            runFlag.clear()
            kill_thread.cancel()
