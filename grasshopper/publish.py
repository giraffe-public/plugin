"""
this will be run by giraffe - provided here as an example
"""
from google.cloud import pubsub_v1

client = pubsub_v1.PublisherClient()
response = client.publish(
    'projects/giraffe-feaso/topics/ttw-plugin',
    ''.encode(),  # empty message/data just use attributes below
    input_url='https://plugins-dot-giraffe-feaso.appspot.com/ttw/1',
    output_url='https://plugins-dot-giraffe-feaso.appspot.com/ttw/1',
)
