import time
from google.cloud import pubsub_v1

# PubSub connection
subscriber = pubsub_v1.SubscriberClient()

def callback(message):
    print('Received message: {}\n'.format(message))
    message.ack()

# Subscribe and only process 1 message at a time
future = subscriber.subscribe(
    'projects/giraffe-feaso/subscriptions/nettletontribe',
    callback=callback,
    flow_control=pubsub_v1.types.FlowControl(max_messages=1)
)

try:
    future.result()
except Exception as ex:
    print(ex)
    print(future.exception())
    subscription.close()
    raise
