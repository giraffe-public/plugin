# plugin

Boilerplate for developing a Rhino Grasshopper based plugin for Giraffe.build. Uses Python 3.7.

The example plugin starts a worker with `grasshopper/start_subscribe.bat` which
* subscribes to [Google Pub/Sub](https://cloud.google.com/pubsub/docs/reference/libraries#client-libraries-install-python) and listens for messages containing `input_url`, `output_url` and 'name'
* GET `input_url` to receive input JSON
* run the Grasshopper script <name>.gh on the input JSON
* POST output JSON to `output_url`
* acknowledge the message

## Usage

At the top of subscribe.py set the following variables in order for you server to start. 
```python
SCRIPT_DIR = 'C:\\Users\\Public\\gh_load'  # GRASSHOPPER SCRIPT <app name>.gh TO GO HERE
INPUT_PATH = 'C:\\Users\\Public\\gh_load\\gmodel.json' # LOAD GMODEL (HAS TO BE SET AT START OF GH SCRIPT)
OUTPUT_PATH = 'C:\\Users\\Public\\gh_dump\\output.json' # DUMP OUTPUT (HAS TO BE SET AT END OF GH SCRIPT)
RESTART_PATH = 'C:\\Users\\Public\\test_plugin\\start_subscribe.bat' # bat file that references credentials json

SUBSCRIPTION_PATH = 'projects/giraffe-feaso/subscriptions/<YOUR_SUBSCRIPTION>' # Given to you with credentials file
```


Python dependencies are listed in `grasshopper/requirements.txt`. Rhino has to be installed.

> #### Note you need a credentials json file from Giraffe in the same folder as subscribe.py and start_subscribe.bat. The name of your credentials file must match start_subscribe.bat


## Testing
To publish a message and test the subscriber run `grasshopper/publish_test.bat`.


## Using Perfmon to Control Rhino Memory Leak

Rhino can leak memory when running Grasshopper scripts. At present, it is unknown whether this is Rhino or the script itself.

To mitigate this, Windows can be configured to kill Rhino and Python and start over when free memory is getting low.
[This](http://techgenix.com/Scripted-Networt-Defense-Part2/) article describes how a script can be triggered by using the Windows utilities "Performance Monitor" & "Event Viewer" when available memory drops below a certain threshold.